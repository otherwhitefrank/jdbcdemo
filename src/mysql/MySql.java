package mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySql
{

    public static void main(String[] args)
    {

        int employeeId;
        try {

            MySql demo = new MySql();

            ResultSet rs = null;
            Connection connection = null;
            Statement statement = null;

            String query = "SELECT * FROM employees";
            try {
                connection = JDBCMySQLConnection.getConnection();
                statement = connection.createStatement();
                rs = statement.executeQuery(query);

                while (rs.next()) {
                    System.out.println("Employee: " + rs.getInt("emp_id"));
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                if (connection != null) {
                    try {
                        connection.close();
                    }
                    catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}